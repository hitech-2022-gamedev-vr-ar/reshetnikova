using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class AudioController : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] private AudioClip shootClip;
    [SerializeField] private AudioClip explosionClip;

    public bool IsPlaying
    {
        get => audioSource.isPlaying;
    }

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        
    }

    private void PlayAudio(AudioClip clip)
    {
        if (clip == null)
        {
            Debug.Log("Couldn't play audio clip. Audio clip is null.");
            return;
        }
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void PlayShootSound()
    {
        PlayAudio(shootClip);
    }

    public void PlayExplosionSound()
    {
        PlayAudio(explosionClip);
    }
}

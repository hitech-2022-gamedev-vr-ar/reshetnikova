using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowballContoler : MonoBehaviour
{
    [SerializeField] private Rigidbody snowBallPrefab;
    [SerializeField] private Transform hand;
    [SerializeField] private float speed = 1f;

    [SerializeField] private float step = 1f;
    private float nextShoot;

    [SerializeField] private LayerMask targetMask;
    [SerializeField] private float distance = 100f;

    void Update()
    {
        if (Physics.Raycast(hand.position, hand.forward, out RaycastHit hit, distance, targetMask)
            && Time.time > nextShoot)
        {
            Shoot();
            nextShoot = Time.time + step;
        }
    }

    void Shoot()
    {
        var snowball = Instantiate(snowBallPrefab, hand.position, hand.rotation);
        snowball.AddForce(snowball.transform.forward * speed, ForceMode.Impulse);
    }
}
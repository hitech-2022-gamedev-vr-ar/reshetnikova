using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(AudioController))]

public class SnowBall : MonoBehaviour
{
    [SerializeField]
    private AudioController audioController;
    private MeshRenderer meshRenderer;
    [SerializeField]
    private ParticleSystem explosionEffect;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        audioController = GetComponent <AudioController>();
        audioController.PlayShootSound();
    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(DestroyExploision());
    }

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(DestroyExploision());
    }

    private IEnumerator DestroyExploision()
    {
        var effect = Instantiate(explosionEffect.gameObject, transform.position, Quaternion.identity);
        ParticleSystem part = effect.GetComponent<ParticleSystem>();
        part.Play();
        Destroy(effect.gameObject, part.main.duration);

        meshRenderer.enabled = false;
        audioController.PlayExplosionSound();
        yield return new WaitUntil(() => audioController.IsPlaying);
        Destroy(gameObject);
    }
}

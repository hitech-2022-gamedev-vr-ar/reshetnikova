using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float rotateSpeed = 10f;
    private Vector3 direction;
    private Vector3 stepRotate;

    void Update()
    {
        direction = target.position - transform.position;
        stepRotate = Vector3.RotateTowards(transform.forward,
            direction, rotateSpeed * Time.deltaTime, 0f);
        transform.rotation = Quaternion.LookRotation(stepRotate);
    }
}
